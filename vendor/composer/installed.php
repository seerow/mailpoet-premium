<?php

if (!defined('ABSPATH')) exit;

 return array(
    'root' => array(
        'name' => '__root__',
        'pretty_version' => 'dev-trunk',
        'version' => 'dev-trunk',
        'reference' => 'adbbfc71b7f6dc00657633e8b0746b13f248c2c7',
        'type' => 'library',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'dev' => false,
    ),
    'versions' => array(
        '__root__' => array(
            'pretty_version' => 'dev-trunk',
            'version' => 'dev-trunk',
            'reference' => 'adbbfc71b7f6dc00657633e8b0746b13f248c2c7',
            'type' => 'library',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
    ),
);
